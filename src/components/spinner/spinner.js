import React from 'react';
import {Text, View} from 'react-native';

export const Spinner = () => {
  return (
    <View>
      <Text>Loading...</Text>
    </View>
  );
};
